<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh Conditional</h2>
    <?php

echo "<h3> Soal No 1 Greetings </h3>";
    /*
    Soal No 1
    Greetings
    Buatlah sebuah function greetings() yang menerima satu parameter berupa string.

    contoh: greetings("abduh");
    Output: "Halo Abduh, Selamat Datang di PKS Digital School!"
    */

    // Code function di sini
    function greetings($nama){
        echo "Hallo" . $nama . ", Selamat Datang di PKS Digital School";
    } 

    // Hapus komentar untuk menjalankan code!
    greetings("Bagas");
    echo "<br>";
    greetings("Wahyu");
    echo "<br>";
    greetings("Abdul");

    echo "<br>";

    
    echo "<h3> Soal No 2<h3>";
    // Code function di sini
    function reverse($kata1){
        $panjangkata = strlen($kata1);
        $tampung = "";
        for($i=($panjangkata - 1); $i>=0; $i--){
            $tampung .= $kata1[$i];
        }
        return $tampung;
    }

    function reverseString($kata2){
        $string=reverse($kata2);
        echo $string . "<br>";
    } 
    // Hapus komentar di bawah ini untuk jalankan Code
     reverseString("abduh");
     reverseString("Digital School");
     reverseString("We Are PKS Digital School Developers");
    echo "<br>";


    echo "<h3>Soal No 3<h3>";
    function polindrome($kata3){
        $balik = reverse($kata3);
        if ($kata3 === $balik) {
            echo "polindrome" . $kata3." => true <br>";
        } else{
            echo "polindrome" . $kata3. " => false <br>";
        }
    }

    // Hapus komentar di bawah ini untuk jalankan Code
     polindrome("civic"); // true
     polindrome("nababan"); // true
     polindrome("jambaban"); // false
     polindrome("racecar"); // true
    echo "<br>";





    echo "<h3>Soal No 4<h3>";        
        // Code function di sini
        function tentukan_nilai($angka){
            $output = "";
            if($angka>=85 && $angka<100) {
                $output.= "Sangat Baik";
            } else if($angka>=70 && $angka<85){
                $output.= "Baik";
            } else if($angka>=60 && $angka<70){
                $output .= "Cukup";
            } else if($angka<60){
                $output .= "Kurang";
            }
            return $output . "<br>";
        }



        // Hapus komentar di bawah ini untuk jalankan code
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
        


?>
</body>
</html>